<?php
/**
 * Simple wrapper class for our canWinGame function
 * TODO: could be extended with other methods useful for the game, such as "suggestNextMove" etc.
 *
 * @author P.Fernihough <paul@spoddycoder.com>
 */
class StoneHeapGame
{

  /**
  * Determine if possible to win the game based on number of stones left
  *
  * @param int $num_stones_left    how many stones left in the heap
  * @return bool                   true if can win, false if cannot
  */
  public static function canWinGame( $num_stones_left ) {

    // can't win if there's a integer multiple of 4 stones left
    return !( $num_stones_left % 4 == 0 );

  }

}
