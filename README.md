# Symless Coding Exercise

## Task
There is a heap of stones on the table, each time one of you take turns to remove 1 to 3 stones.
The one who removes the last stone will be the winner.
You will always take the first turn.
Write a function using PHP to determine whether you can win the game given the number of stones in the heap.

## Analysis
* 0 stones = can't win, opponent has already won!
* 1 stones = can win, remove 1 stone
* 2 stones = can win, remove 2 stones
* 3 stones = can win, remove 3 stones
* 4 stones = can't win, because we can only force opponent into a winning position by removing 1-3 stones
* 5 stones = can win, remove 1 stone to force opponent into the 4 stone losing position
* 6 stones = can win, remove 2 stones to force opponent into the 4 stone losing position
* 7 stones = can win, remove 3 stones to force opponent into the 4 stone losing position
* 8 stones = can't win, because we can only force opponent into a winning position by removing 1-3 stones
* 9 stones = can win, remove 1 stone to force opponent into the 8 stone losing position
* 10 stones = can win, remove 2 stones to force opponent into the 8 stone losing position

The pattern repeats - an integer multiple of 4 stones means we are in a losing position

## Usage
1. Include the StoneHeapGame class in your app
1. Use the StoneHeapGame::canWinGame method to determine if the game is winnable
1. PHPUnit tests included in tests directory
1. Run `composer update` to pull the PHPUnit dependencies
1. Run`./vendor/bin/phpunit` to run tests
