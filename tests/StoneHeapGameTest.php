<?php

use PHPUnit\Framework\TestCase;

/**
 * PHPUnit tests for the StoneHeapGame class
 *
 * @author P.Fernihough <paul@spoddycoder.com>
 */
class StoneHeapGameTest extends TestCase
{

  /**
   * test for the canWinGame method
   *
   * @param int $num_stones_left   number of stones left in heap
   * @param bool $expected_result  whether this many stones can win
   *
   * @dataProvider providerTestCanWinGame
   */
  public function testCanWinGame( $num_stones_left, $expected_result ) {

    $actual_result = StoneHeapGame::canWinGame( $num_stones_left );
    $this->assertEquals( $expected_result, $actual_result );

  }

  /**
   * dataProvider for testCanWinGame with test data and results from original analysis (see README)
   */
  public function providerTestCanWinGame() {

    return array(
      array(0, false),
      array(1, true),
      array(2, true),
      array(3, true),
      array(4, false),
      array(5, true),
      array(6, true),
      array(7, true),
      array(8, false),
      array(9, true),
      array(10, true),
    );

  }

}
